import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import contacts from './store/reducers/contacts'
import rooms from './store/reducers/rooms'
import messages from './store/reducers/messages'
import profile from './store/reducers/profile'
import App from './App'
import * as serviceWorker from './serviceWorker'
import thunk from 'redux-thunk'
import localStorage from './middlewares/localStorage'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const rootReducer = combineReducers({
  contacts,
  rooms,
  messages,
  profile
})

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk, localStorage)
))

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(app, document.getElementById('root'))

serviceWorker.unregister()
