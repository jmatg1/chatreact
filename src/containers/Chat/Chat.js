import React, { Component } from 'react'
import './styles.scss'

import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import * as actions from '../../store/actions/index'

import MessageList from '../../components/MessageList/MessageList'
import ContactList from '../../components/ContactList/ContactList'
import Contact from '../../components/ContactList/Contact/Contact'
import RoomList from '../../components/RoomList/RoomList'
import Room from '../../components/RoomList/Room/Room'
import { randomId } from '../../shared/utility'
import Popup from '../../components/Popup/Popup.js'

class Chat extends Component {
  state = {
    sendId: null,
    isRoom: null,
    showSaveBtn: null,
    showCheckboxes: false,
    checkboxes: {},
    popup: {
      id: null,
      show: false,
      name: null,
      text: null
    }
  }
  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevState.popup.show === this.state.popup.show) { return false }
  }

  /**
   * Генерируется массив <Contact/>
   * @return {Array}
   */
  getContacts = () => {
    const { contacts, messages } = this.props
    const profileId = this.props.profile.id
    const { checkboxes, showCheckboxes } = this.state
    let contactsArr = []
    // let lastNewMessage = null

    for (let id in contacts) {
      let countNewMessages = 0
      if (contacts[id].id === profileId) continue

      for (let idMes in messages) {
        if (messages[idMes].isRead === false && messages[idMes].sendId === profileId && messages[idMes].userId === contacts[id].id) {
          countNewMessages++
        }
      }

      contactsArr.push(
        <Contact key={contacts[id].id}
          id={contacts[id].id}
          name={contacts[id].name}
          checkbox={checkboxes[id]}
          handleCheckbox={this.handleChangeCheckbox(+id)}
          showCheckboxes={showCheckboxes}
          countNewMessages={countNewMessages}
        />
      )
    }

    return contactsArr
  }
  /**
   * Генерируется массив <Room/>
   * @return {Array}
   */
  getRooms = () => {
    const { rooms } = this.props
    const profileId = this.props.profile.id
    const { showSaveBtn } = this.state
    let roomsArr = []

    for (let id in rooms) { // добавляем в массив только комнаты в которых есть пользователь
      if (rooms[id].usersId.some(idInRoom => idInRoom === profileId)) {
        const roomId = rooms[id].id
        const isICreated = rooms[id].createId === profileId

        roomsArr.push(
          <Room key={roomId}
            id={roomId}
            name={rooms[id].name}
            handleEdit={this.handleEditRoom(roomId)}
            handleSave={this.handleSaveRoom(roomId)}
            handleLogout={this.handleLogoutRoom(roomId, isICreated)}
            isICreate={isICreated}
            showSaveBtn={showSaveBtn === roomId}
          />
        )
      }
    }
    return roomsArr
  }
  /**
   * Редактирование комнаты: добавить, удалить участника
   * изменение чекбоксов у контактов
   */
  handleEditRoom = (currentIdRoom) => () => {
    const { contacts, rooms } = this.props
    const checkboxes = {}

    for (let id in contacts) {
      checkboxes[id] = {
        id: id,
        isSelected: rooms[currentIdRoom].usersId.some(userIdRoom => userIdRoom === +id)
      }
    }
    this.setState({
      checkboxes,
      showSaveBtn: currentIdRoom,
      showCheckboxes: true
    })
  }
  /**
   * Сохранить комнату. Клик срабатывает в <Room/>
   */
  handleSaveRoom = (currentIdRoom) => () => {
    const { checkboxes } = this.state
    const usersSelected = []

    for (let id in checkboxes) {
      if (checkboxes[id].isSelected) {
        usersSelected.push(+id)
      }
    }

    this.setState({
      showSaveBtn: false,
      showCheckboxes: false
    })
    this.props.editRoom(currentIdRoom, usersSelected)
  }
  /**
   * Выйти из комнаты. Клик срабатывает в <Room/>
   * Если хозяин комнаты - распускает(удаляет) её
   */
  handleLogoutRoom = (currentIdRoom, isICreated) => () => {
    this.props.removeRoom(currentIdRoom, this.props.profile.id, isICreated)
    this.setState({
      showSaveBtn: null,
      showCheckboxes: false
    })
  }
  /**
   * Обработка чекбоксов в <Contact/>
   * @param checkboxId {number}
   */
  handleChangeCheckbox = (checkboxId) => () => {
    const checkboxes = { ...this.state.checkboxes }
    checkboxes[checkboxId].isSelected = !checkboxes[checkboxId].isSelected

    this.setState({ checkboxes })
  }
  /**
   * Генерируется <MessageList/> В нем сообщения <Message>
   * @param sendId {number}  id комнаты или собеседника
   * @param isRoom {boolean}  комната это
   * @return {*}
   */
  renderMessageList = (sendId, isRoom = false) => {
    return <MessageList id={sendId}
      isRoom={isRoom}
      profileId={this.props.profile.id}
    />
  }
  /**
   * Добавляет комнату. Клик срабатывает в <Room/>
   */
  handleAddRoom = () => {
    const profileId = this.props.profile.id
    const nameRoom = window.prompt('Введите имя комнаты')
    if (nameRoom.length === 0) return alert('Имя не может быть пустым')
    const newRoom = {
      id: randomId(),
      createId: profileId,
      name: nameRoom,
      usersId: [profileId]
    }
    this.props.addRoom(newRoom)
  }

  render () {
    const { name } = this.props.profile
    const { popup } = this.props
    return (
      <div className="chat">
        <div className="chat__left">
          <div className="my-profile">{name}</div>
          <ContactList>
              {this.getContacts()}
          </ContactList>
          <RoomList handleAddRoom={this.handleAddRoom}>
            {this.getRooms()}
          </RoomList>
        </div>
        <div className="chat__right">
          <Switch>
            <Route path="/user/:id" render={({ match }) => this.renderMessageList(+match.params.id)}/>
            <Route path="/room/:id" render={({ match }) => this.renderMessageList(+match.params.id, true)}/>
            <Route path="/" render={() => <h1>Выберите, кому хотели бы написать</h1>}/>
          </Switch>
        </div>
        {popup.show ? <Popup popup={popup} /> : null }
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { contacts, messages } = state
  const profileId = state.profile.id

  let lastNewMessage = null
  let popup = {
    show: false
  }

  for (let id in contacts) {
    if (contacts[id].id === profileId) continue

    for (let idMes in messages) {
      if (messages[idMes].isRead === false && messages[idMes].sendId === profileId && messages[idMes].userId === contacts[id].id) {
        lastNewMessage = messages[idMes]
        lastNewMessage.userName = contacts[messages[idMes].userId].name
      }
    }

    if (lastNewMessage) {
      popup = {
        id: lastNewMessage.userId,
        show: true,
        name: lastNewMessage.userName,
        text: lastNewMessage.text
      }
    }
  }
  return {
    profile: state.profile,
    contacts: state.contacts,
    rooms: state.rooms,
    messages: state.messages,
    popup
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setProfile: (profile) => dispatch(actions.setProfile(profile)),
    editRoom: (id, usersId) => dispatch(actions.socketEditRoom(id, usersId)),
    removeRoom: (roomId, userId, isICreated) => dispatch(actions.socketRemoveRoom(roomId, userId, isICreated)),
    addRoom: (room) => dispatch(actions.socketAddRoom(room))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
