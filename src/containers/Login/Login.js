import React, { Component } from 'react'
import './styles.scss'

import * as actions from '../../store/actions/index'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { updateObject, randomId } from '../../shared/utility'
class Login extends Component {
  state = {
    controls: {
      name: {
        type: 'text',
        placeholder: 'Имя',
        value: ''
      },
      password: {
        type: 'password',
        placeholder: 'Пароль',
        value: ''
      }
    },
    methodAuth: 'login',
    isSignup: false
  }
  /**
   * Заносит value в интуп
   * @param event
   * @param controlName
   */
  handleInput = (event, controlName) => {
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event.target.value
      })
    })

    this.setState({ controls: updatedControls })
  }
  /**
   * Обработка input[radio]
   * Выбор между авторизацией и регистрацией
    * @param ev
   */
  handleRadio = (ev) => this.setState({ methodAuth: ev.target.value })

  /**
   * Обработка отправки формы авторизации
   * Проверяет зарегистрирован ли пользователь
   * Добавляет нового в contacts
   * @param ev
   */
  handleFormSubmit = (ev) => {
    ev.preventDefault()
    const contacts = this.props.contacts
    let profile = {
      id: randomId(),
      name: this.state.controls.name.value,
      password: this.state.controls.password.value
    }
    // регистрация
    if (this.state.methodAuth === 'reg') {
      let isNewContact = false

      for (let id in contacts) { // проверка на существующего пользователя
        if (contacts[id].name === profile.name) { isNewContact = true; break }
      }

      if (isNewContact) {
        alert('Логин занят')
        return
      } else {
        this.props.addContact(profile)
        this.props.setProfile(profile)
      }
    } else { // авторизация
      let isContact = false

      for (let id in contacts) { // поиск имени и пароля
        if (contacts[id].name === profile.name && contacts[id].password === profile.password) {
          profile.id = contacts[id].id
          isContact = true
        }
      }

      if (isContact) { // авторизируемся
        delete profile['password']
        this.props.setProfile(profile)
      } else {
        alert('Неверный логин или пароль')
        return
      }
    }
    // после успешной авторизации или регистрации, перенаправляем на главную
    this.props.history.replace('/')
  }

  render () {
    const { methodAuth } = this.state
    const formElementsArray = []
    for (let key in this.state.controls) { // генерируем массив данных элементов формы
      formElementsArray.push({
        id: key,
        config: this.state.controls[key]
      })
    }

    let form = formElementsArray.map(formElement => ( // генерируем массив инпутов формы
      <input
        key={formElement.id}
        className="auth__input"
        type={formElement.config.type}
        value={formElement.config.value}
        placeholder={formElement.config.placeholder}
        required={true}
        onChange={(event) => this.handleInput(event, formElement.id)}/>
    ))
    return (
      <div className="auth">
        <form className="auth__form" onSubmit={this.handleFormSubmit}>
          <fieldset>
            <label>
              Войти
              <input type="radio" value="login" checked={methodAuth === 'login'}
                onChange={this.handleRadio}/>
            </label>
            <label>
              Регистрация
              <input type="radio" value="reg" checked={methodAuth === 'reg'} onChange={this.handleRadio}/>
            </label>
          </fieldset>
          {form}
          <button className="auth__btn-send">Отправить</button>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  }
}

const mapDispatchToState = dispatch => {
  return {
    addContact: (contact) => dispatch(actions.socketAddContact(contact)),
    setProfile: (profile) => dispatch(actions.setProfile(profile))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToState)(Login))
