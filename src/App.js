import React, { Component } from 'react'
import './App.scss'
import './css/reset.css'

import { Route, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { socketsConnect } from './store/actions/index'
import Chat from './containers/Chat/Chat'
import Login from './containers/Login/Login'

class App extends Component {
  componentDidMount () {
    this.props.socket()
  }
  render () {
    console.log('RENDER APP')

    return (
      <div className="container">
        <Switch>
          <Route path="/auth" component={Login}/>
          {this.props.profile.id === null ? <Redirect to="/auth" component={Login}/> : '<Redirect to="/" component={Login} />'}
          <Route path="/" component={Chat}/>

        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    profile: state.profile
  }
}

const mapDispatchToProps = dispatch => {
  return {
    socket: () => dispatch(socketsConnect())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
