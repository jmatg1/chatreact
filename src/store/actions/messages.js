import * as actionTypes from './actionTypes'

export const removeMessage = id => {
  return {
    type: actionTypes.REMOVE_MESSAGE,
    id
  }
}

export const editMessage = (id, text) => {
  return {
    type: actionTypes.EDIT_MESSAGE,
    id,
    text
  }
}
export const readMessages = (listMessageRead) => {
  return {
    type: actionTypes.READ_MESSAGES,
    listMessageRead
  }
}

export const addMessage = (message) => {
  return {
    type: actionTypes.ADD_MESSAGE,
    message
  }
}
