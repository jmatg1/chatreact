import SocketIOClient from 'socket.io-client'
import * as actionTypes from './actionTypes'
import * as disp from './index'
let socket = null
export const socketsConnect = () => {
  return (dispatch, getState) => {
    dispatch({
      type: actionTypes.SOCKETS_CONNECTION_REQUEST
    })

    socket = SocketIOClient('http://localhost:8080')

    socket.on('connect', () => {
      dispatch({
        type: actionTypes.SOCKETS_CONNECTION_SUCCESS
      })
    })

    console.log('socker')

    socket.on('error', () => {
      dispatch({
        type: actionTypes.SOCKETS_CONNECTION_FAILURE
      })
    })

    socket.on('connect_error', () => {
      dispatch({
        type: actionTypes.SOCKETS_CONNECTION_FAILURE
      })
    })

    socket.on('messages', (messages) => {
      dispatch({
        type: actionTypes.SOCKETS_FETCH_MESSAGES,
        messages
      })
    })

    socket.on('contacts', (contacts) => {
      dispatch({
        type: actionTypes.SOCKETS_FETCH_CONTACTS,
        contacts
      })
    })

    socket.on('rooms', (rooms) => {
      dispatch({
        type: actionTypes.SOCKETS_FETCH_ROOMS,
        rooms
      })
    })
  }
}

export function socketSendMessage (message) {
  return (dispatch, getState) => {
    socket.emit('send-message', message)
    dispatch(disp.addMessage(message))
  }
}
export function socketEditMessage (id, text) {
  return (dispatch, getState) => {
    socket.emit('edit-message', { id, text: text })
    dispatch(disp.editMessage(id, text))
  }
}
export function socketReadMessages (listMessageRead) {
  return (dispatch, getState) => {
    socket.emit('read-message', listMessageRead)
    dispatch(disp.readMessages(listMessageRead))
  }
}
export function socketAddContact (contact) {
  console.log('send-contact')

  return (dispatch, getState) => {
    socket.emit('send-contact', contact)
    dispatch(disp.addContact(contact))
  }
}
export function socketAddRoom (room) {
  return (dispatch) => {
    socket.emit('add-room', room)
    dispatch(disp.addRoom(room))
  }
}
export function socketEditRoom (id, usersId) {
  return (dispatch) => {
    socket.emit('edit-room', { id: id, usersId: usersId })
    dispatch(disp.editRoom(id, usersId))
  }
}
export function socketRemoveRoom (roomId, userId, isICreated) {
  return (dispatch) => {
    socket.emit('remove-room', { roomId, userId, isICreated })
    dispatch(disp.removeRoom(roomId, userId, isICreated))
  }
}
