import * as actionTypes from './actionTypes'

export const editRoom = (id, usersId) => {
  return {
    type: actionTypes.EDIT_ROOM,
    id,
    usersId
  }
}

export const removeRoom = (roomId, userId, isICreated) => {
  return {
    type: actionTypes.REMOVE_ROOM,
    userId,
    roomId,
    isICreated
  }
}
export const addRoom = (room) => {
  return {
    type: actionTypes.ADD_ROOM,
    room
  }
}
