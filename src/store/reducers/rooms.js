import * as actionTypes from '../actions/actionTypes'
import { arrayToObject } from '../../shared/utility'

const fetchRooms = (state, action) => {
  return {
    ...arrayToObject(action.rooms)
  }
}

const editRoom = (state, action) => {
  const { id, usersId } = action
  const updateState = { ...state }
  updateState[id].usersId = usersId

  return {
    ...state,
    ...updateState
  }
}
const addRoom = (state, action) => {
  const { room } = action

  return {
    ...state,
    [room.id]: { ...room }
  }
}
const removeRoom = (state, action) => {
  const { roomId, userId, isICreated } = action
  let rooms = { ...state }

  if (isICreated) {
    delete rooms[roomId]
    return {
      ...rooms
    }
  }
  let room = { ...rooms[roomId] }
  let array = [...room.usersId]
  array = array.filter(x => x !== userId)
  room.usersId = [...array]
  rooms[roomId] = { ...room }
  return {
    ...rooms
  }
}

const rooms = (state = null, action) => {
  switch (action.type) {
  case actionTypes.EDIT_ROOM:
    return editRoom(state, action)
  case actionTypes.REMOVE_ROOM:
    return removeRoom(state, action)
  case actionTypes.ADD_ROOM:
    return addRoom(state, action)
  case actionTypes.SOCKETS_FETCH_ROOMS:
    return fetchRooms(state, action)

  default:
    return state
  }
}

export default rooms
