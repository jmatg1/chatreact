import * as actionTypes from '../actions/actionTypes'
import { arrayToObject } from '../../shared/utility'

const fetchMessages = (state, action) => {
  return {
    ...arrayToObject(action.messages)
  }
}

const editMessage = (state, action) => {
  const { id, text } = action
  const messages = { ...state }

  if (text.length === 0) {
    delete messages[id]

    return {
      ...messages
    }
  }

  return {
    ...state,
    [id]: {
      ...state[id],
      text
    }
  }
}
const addMessage = (state, action) => {
  const { message } = action

  return {
    ...state,
    [message.id]: {
      ...message
    }
  }
}
const readMessage = (state, action) => {
  const { listMessageRead } = action
  let updateMessages = {...state}
  listMessageRead.map(id => updateMessages[id].isRead = true)

  return {
    ...state,
    updateMessages
  }
}

const messages = (state = null, action) => {
  switch (action.type) {
  case actionTypes.EDIT_MESSAGE: return editMessage(state, action)
  case actionTypes.ADD_MESSAGE: return addMessage(state, action)
  case actionTypes.READ_MESSAGES: return readMessage(state, action)
  case actionTypes.SOCKETS_FETCH_MESSAGES: return fetchMessages(state, action)
  default:
    return state
  }
}

export default messages
