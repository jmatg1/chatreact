import * as actionTypes from '../actions/actionTypes'
import { arrayToObject } from '../../shared/utility'

const fetchContacts = (state, action) => {
  return {
    ...arrayToObject(action.contacts)
  }
}

const addContact = (state, action) => {
  const newContact = { ...action.contact }

  return {
    ...state,
    [newContact.id]: { ...newContact }
  }
}

const contacts = (state = null, action) => {
  switch (action.type) {
  case actionTypes.ADD_CONTACT: return addContact(state, action)
  case actionTypes.SOCKETS_FETCH_CONTACTS: return fetchContacts(state, action)

  default:
    return state
  }
}

export default contacts
