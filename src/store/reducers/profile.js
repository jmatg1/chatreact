import * as actionTypes from '../actions/actionTypes'

const initialState = {
  id: null,
  sendId: null,
  isRoom: null,
  name: null
}

const setProfile = (state, action) => {
  return {
    ...state,
    ...action.profile
  }
}

const profile = (state = initialState, action) => {
  switch (action.type) {
  case actionTypes.SET_PROFILE: return setProfile(state, action)
  default: return state
  }
}

export default profile
