var http = require('http')
var fs = require('fs')

let contactsJson = JSON.parse(fs.readFileSync('./src/socket/data/contacts.json'))
let messagesJson = JSON.parse(fs.readFileSync('./src/socket/data/messages.json'))
let roomsJson = JSON.parse(fs.readFileSync('./src/socket/data/rooms.json'))

function saveDate (name, path) {
  let data = JSON.stringify(name)
  const fullPath = './src/socket/data/' + path + '.json'
  fs.writeFileSync(fullPath, data)
}

// Загружаем файл index.html и отображаем его клиенту
var server = http.createServer(function (req, res) {
  fs.readFile('./src/socket/index.html', 'utf-8', function (error, content) {
    console.log('server')
    res.writeHead(200, { 'Content-Type': 'text/html' })
    res.end(content)
  })
})

// Загружаем socket.io
var io = require('socket.io').listen(server)

// Когда клиент соединяется, выводим сообщение в консоль

io.sockets.on('connection', function (socket) {
  console.log('connect')
  socket.emit('messages', messagesJson)
  socket.emit('contacts', contactsJson)
  socket.emit('rooms', roomsJson)

  socket.broadcast.on('send-message', function (message) {
    messagesJson.push(message)
    saveDate(messagesJson, 'messages')
    socket.broadcast.emit('messages', messagesJson)
  })

  socket.broadcast.on('edit-message', function ({ id, text }) {
    console.log(id, text)
    if (text.length === 0) {
      let delIndex = null
      messagesJson.map((el, i) => {
        if (el.id === id) {
          delIndex = i
        }
      })
      if (delIndex === null) return false
      messagesJson.splice(delIndex, 1)
    } else {
      messagesJson.map(el => el.id === id ? el.text = text : el)
    }

    saveDate(messagesJson, 'messages')
    console.log('edit-message')
    socket.broadcast.emit('messages', messagesJson)
  })

  socket.broadcast.on('read-message', function (listMessageRead) {
    console.log('read-message')

    messagesJson.map((mes, i) => {
      if (listMessageRead.some(_id => _id === mes.id)) {
        messagesJson[i].isRead = true
      }
    })

    saveDate(messagesJson, 'messages')
    console.log('edit-message')
    socket.broadcast.emit('messages', messagesJson)
  })

  socket.broadcast.on('send-contact', function (contacts) {
    contactsJson.push(contacts)
    saveDate(contactsJson, 'contacts')
    console.log(contactsJson)
    socket.broadcast.emit('contacts', contactsJson)
  })

  socket.broadcast.on('add-room', function (rooms) {
    roomsJson.push(rooms)
    saveDate(roomsJson, 'rooms')
    console.log('add-room')
    socket.broadcast.emit('rooms', roomsJson)
  })

  socket.broadcast.on('edit-room', function ({ id, usersId }) {
    console.log('edit-room')
    roomsJson.map(el => el.id === id ? el.usersId = usersId : el)

    saveDate(roomsJson, 'rooms')
    socket.broadcast.emit('rooms', roomsJson)
  })

  socket.broadcast.on('remove-room', function ({ roomId, userId, isICreated }) {
    console.log('remove-room')
    let delIndex = null

    if (isICreated) {
      roomsJson.map((el, i) => {
        if (el.id === roomId) {
          delIndex = i
        }
      })
      if (delIndex === null) return false
      roomsJson.splice(delIndex, 1)
    } else {
      roomsJson.map(el => el.id === roomId ? el.usersId = el.usersId.filter(x => x !== userId) : el)
    }

    console.log(delIndex)

    saveDate(roomsJson, 'rooms')
    socket.broadcast.emit('rooms', roomsJson)
  })
})

server.listen(8080)
