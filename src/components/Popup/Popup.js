import React from 'react'
import './styles.scss'
import { NavLink } from 'react-router-dom'

const popup = (props) => {
  const { id, name, text, handleClose } = props.popup
  return (
    <div className="one_pm ">
      <div className="h_newpm">
        <div className="h_npin"><NavLink to={`/user/${id}`} className="h_nover"/>
          <div className="h_nptitle">Новое сообщение</div>
          <span className="h_del" onClick={handleClose}/>
          <div className="h_npl">
            <div className="h_npimg"><NavLink to={`/user/${id}`} id="hnp_avatar"/><img src="https://yraaa.ru/images/noavatar.gif" alt=""/></div>
          </div>
          <div className="h_npr"><NavLink to={`/user/${id}`} id="hnp_author">{name}</NavLink> <a id="hnp_text">{text}</a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default popup
