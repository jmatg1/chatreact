import React from 'react'
import './styles.scss'
import PropTypes from 'prop-types'

const contactList = props => {
  const { children } = props
  return (
    <div className="contacts">
      <ul className="contacts__list">
        {children}
      </ul>
    </div>

  )
}

contactList.propTypes = {
  children: PropTypes.node.isRequired
}

export default contactList
