import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from '../../Checkbox/Checkbox'
import { NavLink } from 'react-router-dom'

const contact = (props) => {
  const { id, name, checkbox, handleCheckbox, showCheckboxes, countNewMessages } = props
  console.log('--render contact')
  return (
    <li className="contacts__item">
      <NavLink to={`/user/${id}`} activeClassName={'selected'}>

        <div className="contacts__name jq-contact">
          {name}

        </div>
      </NavLink>
      { countNewMessages !== 0 ? <div className="contacts__new-message">{countNewMessages}</div> : null}
      { showCheckboxes
        ? <Checkbox label={checkbox.id} isSelected={checkbox.isSelected} onCheckboxChange={handleCheckbox} />
        : null }
    </li>
  )
}

contact.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  checkbox: PropTypes.shape({
    id: PropTypes.number.isRequired.isRequired,
    isSelected: PropTypes.bool.isRequired
  }),
  handleCheckbox: PropTypes.func.isRequired,
  showCheckboxes: PropTypes.bool.isRequired
}

export default contact
