import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './styles.scss'

import { connect } from 'react-redux'
import * as actions from '../../store/actions/index'

class MessageSend extends Component {
  static propTypes = {
    sendId: PropTypes.number.isRequired,
    isRoom: PropTypes.bool.isRequired,
    profileId: PropTypes.number.isRequired
  }

  state = {
    textComment: ''
  }

  handleSendMessage = (ev) => {
    if (ev) ev.preventDefault()

    const { sendId, isRoom, profileId } = this.props
    const message = {
      id: Math.floor(Date.now() + Math.random()),
      sendId: isRoom ? null : sendId,
      userId: profileId,
      roomId: isRoom ? sendId : null,
      text: this.state.textComment,
      dateCreate: new Date().toJSON(),
      isRead: false
    }

    this.props.addMessage(message)
    this.setState({ textComment: '' })
  }
  handleInputMessage = (ev) => {
    this.setState({ textComment: ev.target.value })
  }
  onEnterPress = (e) => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault()
      this.handleSendMessage()
    }
  }
  render () {
    const { textComment } = this.state

    return (
      <form className="message-send" ref={el => this.myFormRef = el} onSubmit={this.handleSendMessage}>
        <textarea className="message-send__input"
          required
          value={textComment}
          onChange={this.handleInputMessage}
          onKeyDown={this.onEnterPress} />
        <button className="message-send__btn">Отправить</button>
      </form>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addMessage: (message) => dispatch(actions.socketSendMessage(message))
  }
}

export default connect(null, mapDispatchToProps)(MessageSend)
