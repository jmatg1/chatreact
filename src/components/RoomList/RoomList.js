import React from 'react'
import './styles.scss'
import PropTypes from 'prop-types'

const roomList = props => {
  const { children, handleAddRoom } = props
  return (
    <div className="rooms">
      <span onClick={handleAddRoom}>Добавить комнату</span>
      <ul className="rooms__list">
        {children}
      </ul>
    </div>

  )
}

roomList.propTypes = {
  children: PropTypes.node.isRequired,
  handleAddRoom: PropTypes.func.isRequired
}

export default roomList
