import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

class Room extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    isICreate: PropTypes.bool.isRequired,
    handleSave: PropTypes.func,
    handleEdit: PropTypes.func,
    handleLogout: PropTypes.func,
    showSaveBtn: PropTypes.bool.isRequired
  }

  state = {
    nameRoom: null,
    showSaveBtn: false
  }

  render () {
    const { id, name, isICreate, handleSave, handleEdit, handleLogout, showSaveBtn } = this.props

    return (
      <li className="rooms__item">
        <NavLink to={`/room/${id}`} activeClassName={'selected'}>
          <div className="jq-room rooms__name">
            {name}
          </div>
        </NavLink>
        <div className="room-control">
          {isICreate ? <>
            <span className="room-control__btn" onClick={handleEdit} style={showSaveBtn ? { display: 'none' } : { display: 'flex' }}>✎</span>
            <span className="room-control__btn" onClick={handleSave} style={showSaveBtn ? { display: 'flex' } : { display: 'none' }}>✔</span>
          </>
            : null}
          <span className="room-control__btn" onClick={handleLogout}>✘</span>
        </div>
      </li>
    )
  }
}

export default Room
