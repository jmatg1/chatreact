import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './styles.scss'

import { connect } from 'react-redux'
import Moment from 'react-moment'
import 'moment/locale/ru'
import * as actions from '../../../store/actions/index'

class Message extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    date: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    isICreate: PropTypes.bool.isRequired
  }

  state = {
    textMessage: '',
    canChange: false
  }

  componentDidMount () {
    this.setState({ textMessage: this.props.text })
  }
  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevProps.text !== this.props.text) {
      this.setState({ textMessage: this.props.text })
    }
  }

  handleChangeCommentOn = () => {
    if (!this.props.isICreate) return
    this.setState({ canChange: true }, () => this._comment.focus())
  }

  handleChangeCommentOf = (ev) => {
    if (!this.props.isICreate) return

    const text = ev.target.textContent
    const { id } = this.props

    this.setState({
      canChange: false,
      textMessage: text
    })

    this.props.editMessage(id, text)
  }

  render () {
    const { name, date, isICreate } = this.props
    const { textMessage, canChange } = this.state
    let classes = ['message']
    let classesComment = ['message__text']

    if (isICreate) classes.push('message--me')
    if (canChange) classesComment.push('message__text--edit')
    return (
      <div className={classes.join(' ')}>
        {name ? <span className="message__name">{name}</span> : null}
        <span className={classesComment.join(' ')}
          ref={(node) => this._comment = node}
          onClick={this.handleChangeCommentOn}
          contentEditable={canChange ? 'true' : 'false'}
          suppressContentEditableWarning={true}
          onBlur={this.handleChangeCommentOf}
        >{textMessage}</span>
        <span className="message__time"> <Moment locale="ru" fromNow>{date}</Moment></span>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    editMessage: (id, text) => dispatch(actions.socketEditMessage(id, text))
  }
}

export default connect(null, mapDispatchToProps)(Message)
