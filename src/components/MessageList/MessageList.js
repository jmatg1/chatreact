import React, { Component } from 'react'
import './styles.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Message from '../MessageList/Message/Message'
import MessageSend from '../MessageSend/MessageSend'
import * as actions from "../../store/actions";

class MessageList extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    isRoom: PropTypes.bool.isRequired,
    profileId: PropTypes.number.isRequired
  }

  componentDidMount () {
    if (this.props.listMessageRead.length > 0) {
      this.props.readMessages(this.props.listMessageRead)
      console.log('Есть не прочитанные')
    }
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if(prevProps.listMessageRead.length !== this.props.listMessageRead.length) {
      this.props.readMessages(this.props.listMessageRead)
      console.log('Читаем не прочитанные')
    }
  }

  getMessages = () => {
    const { messages, contacts, isRoom } = this.props
    const profileId = this.props.profile.id
    let messageArr = []

    for (let id in messages) {
      messageArr.push(
        <Message
          key={messages[id].id}
          id={messages[id].id}
          name={messages[id].userId === profileId || isRoom === false ? null : contacts[messages[id].userId].name }
          date={messages[id].dateCreate}
          text={messages[id].text}
          isICreate={messages[id].userId === profileId}/>
      )
    }
    return messageArr
  }

  render () {
    const { id, isRoom, profileId } = this.props
    return (
      <>
        <div className="message-list">
          {this.getMessages()}
        </div>
        <MessageSend sendId={id} isRoom={isRoom} profileId={profileId} />
      </>
    )
  }
}

const mapStateToProps = (state, props) => {
  const allMessages = state.messages
  const { id: currentId, isRoom } = props // currentId -  комнаты или пользователя
  const profileId = state.profile.id
  let messages = []
  let listMessageRead = []

  if (isRoom) {
    for (let id in allMessages) {
      if (allMessages[id].roomId === currentId) {
        if (allMessages[id].isRead === false) listMessageRead.push(allMessages[id].id)
        messages.push(allMessages[id])
      }
    }
  } else {
    for (let id in allMessages) {
      if (allMessages[id].userId === currentId &&
        allMessages[id].sendId === profileId ||
        allMessages[id].sendId === currentId &&
        allMessages[id].userId === profileId) {
        if (allMessages[id].isRead === false && allMessages[id].userId !== profileId) listMessageRead.push(allMessages[id].id)
        messages.push(allMessages[id])
      }
    }
  }
  return {
    messages: messages,
    profile: state.profile,
    contacts: state.contacts,
    rooms: state.rooms,
    listMessageRead
  }
}

const mapDispatchStateToProps = dispatch => {
  return {
    readMessages: (listMessageRead) => dispatch(actions.socketReadMessages(listMessageRead))
  }
}

export default connect(mapStateToProps, mapDispatchStateToProps)(MessageList)
