import React from 'react'
import './styles.scss'
import PropTypes from 'prop-types'

const checkbox = ({ label, isSelected, onCheckboxChange }) => (
  <input
    className="checkbox"
    type="checkbox"
    name={label}
    checked={isSelected}
    onChange={onCheckboxChange}
  />
)
checkbox.propTypes = {
  label: PropTypes.string,
  isSelected: PropTypes.bool,
  onCheckboxChange: PropTypes.func
}

export default checkbox
